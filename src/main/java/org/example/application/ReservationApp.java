package org.example.application;

import org.example.domain.model.Aggregate.Reservation;
import org.example.domain.model.Entity.*;
import org.example.domain.model.ValueObject.Bedroom;
import org.example.domain.model.ValueObject.Car;
import org.example.domain.repository.Catalog;
import org.example.domain.model.Entity.service.BedroomReservation;
import org.example.domain.model.Entity.service.CarReservation;
import org.example.domain.model.Entity.service.FlightReservation;
import org.example.domain.model.ValueObject.type.YDTCity;
import org.example.infrastructure.ReservationRepositoryInMemory;

import java.util.*;

/**
 *  This class belongs to the application layer (COUCHE APPLICATION)
 */
public class ReservationApp {
    ReservationRepositoryInMemory resInMemory = new ReservationRepositoryInMemory();
    Catalog catalog = new Catalog();

    public Catalog getCatalog() {
        return catalog;
    }


    /**
     * Create a simple reservation without service
     * @param name the name of the customer
     * @param departure the departure
     * @param arrival the arrival or destination
     * @param trajectory the chosen travel route
     * @return return the reservation id;
     */
    public int createReservationWithoutService(String name, int departure, int arrival, int trajectory) {
        Customer customer = new Customer(name);
        YDTCity departureTown = YDTCity.getValueCity(departure);
        YDTCity arrivalTown = YDTCity.getValueCity(arrival);
        HashMap<Integer, FlightReservation> routeAvailable = catalog.getFlightRouteAvailable(departureTown, arrivalTown);
        FlightReservation route = routeAvailable.get(trajectory);
        Reservation reservation = new Reservation(customer, route);
        resInMemory.save(reservation);
        int id = reservation.getReservationID();
        System.out.println(id);
        return id;
    }

    /**
     * Create reservation with simple service
     * @param name name of the customer
     * @param departure the departure airport
     * @param arrival the arrival airport
     * @param trajectory the route
     * @param hotelID chosen hotel id
     * @param bedroomID chosen bedroom id
     * @param numberOfDayBedroom number of days in the room
     * @param carRentCompanyID chosen rent car company id
     * @param carID chosen car id
     * @param numberOfDayCar number of days with the car
     * @return reservation id if it created or null otherwise
     */
    public int createReservationWithServiceSimple(String name, int departure, int arrival, int trajectory, int hotelID, int bedroomID, int numberOfDayBedroom, int carRentCompanyID, int carID, int numberOfDayCar){
        Customer customer;
        customer = new Customer(name);
        YDTCity departureTown = YDTCity.getValueCity(departure);
        YDTCity arrivalTown = YDTCity.getValueCity(arrival);
        HashMap<Integer, FlightReservation> routeAvailable = catalog.getFlightRouteAvailable(departureTown, arrivalTown);
        FlightReservation route = routeAvailable.get(trajectory);
        HashMap<Integer, Hotel> hotelInDestinations = catalog.returnHotelInDestination(YDTCity.getValueCity(arrival));
        Hotel selectedHotel = hotelInDestinations.get(hotelID);
        Bedroom freeBedRoom = selectedHotel.getFreeBedrooms().get(bedroomID).getBedroom();
        BedroomReservation bedroomReservation = new BedroomReservation(freeBedRoom, numberOfDayBedroom);
        HashMap<Integer, CarRentCompany> carRentCompanyInDestination = catalog.returnCarRentCompanyInDestination(YDTCity.getValueCity(arrival));
        CarRentCompany carRentCompanySelected = carRentCompanyInDestination.get(carRentCompanyID);
        Car car = carRentCompanySelected.getFreeCars().get(carID).getCar();
        CarReservation carReservation = new CarReservation(car, numberOfDayCar);
        SimpleService service = new SimpleService(bedroomReservation, carReservation);
        ReservationWithServices reservation = new ReservationWithServices(customer, route, service);
        resInMemory.save(reservation);
        int id = reservation.getReservationID();
        System.out.println(id);
        return id;
    }

    public void createReservationWithDeluxeService(int departure, int arrival, int hotel, int carRentCompany, int bedroom, int car){

    }

    //Affichage

    /**
     * We fill in the ID of the reservation and the method displays it to us
     * @param reservation id of the reservation
     */
    public void printReservation(int reservation) {
        StringBuilder sb = new StringBuilder();
        Reservation reservationInMemory = resInMemory.findReservationById(reservation);
        System.out.println(reservationInMemory);
        sb.append(reservationInMemory);
    }

    /**
     * We fill in the departure and de arrival airport and the method displays possible routes to us
     * @param departure departure airport
     * @param arrival destination airport
     * @return display routes
     */
    public String printAvailableRoute(int departure, int arrival) {
        HashMap<Integer, FlightReservation> routeAvailable = catalog.getFlightRouteAvailable(YDTCity.getValueCity(departure), YDTCity.getValueCity(arrival));
        StringBuilder sb = new StringBuilder();
        for (FlightReservation flightReservation : routeAvailable.values()){
            sb.append(flightReservation);
        }
        return sb.toString();
    }

    /**
     * Display available departure and destination
     * @return String
     */
    public String printDeparture(){
        StringBuilder departure = new StringBuilder();
        YDTCity[] cities =  YDTCity.values();
        for (YDTCity ydtCity : cities) {
            int cityIndex = ydtCity.ordinal();
            cityIndex++;
            departure.append("          ").append(cityIndex).append("- ").append(ydtCity).append("\n");
        }
        return departure.toString();
    }

    /**
     * We fill in the destination and methode display all hotels in this destination
     * @param arrival destination town
     * @return display airport
     */
    public String printAvailableHotelInDestination(int arrival) {
        HashMap<Integer, Hotel> hotelInDestination = catalog.returnHotelInDestination(YDTCity.getValueCity(arrival));
        StringBuilder sb = new StringBuilder();
        for (Hotel hotel : hotelInDestination.values()) {
            sb.append(hotel).append("\n");
        }
        return sb.toString();
    }

    /**
     * We fill in the destination and methode display all car rent company in this destination
     * @param arrival destination town
     * @return display car rent company
     */
    public String printAvailableCarRentCompany(int arrival) {
        HashMap<Integer, CarRentCompany> carRentCompanyInDestination = catalog.returnCarRentCompanyInDestination(YDTCity.getValueCity(arrival));
        StringBuilder sb = new StringBuilder();
        for (CarRentCompany carRentCompany : carRentCompanyInDestination.values()) {
            sb.append(carRentCompany).append("\n");
        }
        return sb.toString();
    }

    /**
     * We fill in the destination and the hotelID and the methode display all free bedrooms in this hotel
     * @param arrival destination town
     * @param hotelID chosen hotel
     * @return free bedrooms in this hotel
     */
    public String printAvailableBedroom(int arrival, int hotelID) {
        HashMap<Integer, Hotel> hotelInDestination = catalog.returnHotelInDestination(YDTCity.getValueCity(arrival));
        HashMap<Integer, BedroomReservation> bedrooms = hotelInDestination.get(hotelID).getFreeBedrooms();
        StringBuilder sb = new StringBuilder();
        for (BedroomReservation bedroom : bedrooms.values()) {
            sb.append(bedroom);
        }
        return sb.toString();
    }

    /**
     *  We fill in the destination and the car rent company id and the methode display all free bedrooms in this hotel
     * @param arrival destination town
     * @param carRentCompanyID chosen car rent company
     * @return free cars in this company
     */
    public String printAvailableCar(int arrival, int carRentCompanyID) {
        HashMap<Integer, CarRentCompany> hotelInDestination = catalog.returnCarRentCompanyInDestination(YDTCity.getValueCity(arrival));
        HashMap<Integer, CarReservation> cars = hotelInDestination.get(carRentCompanyID).getFreeCars();
        StringBuilder sb = new StringBuilder();
        for (CarReservation car : cars.values()) {
            sb.append(car);
        }
        return sb.toString();
    }


    //Verification

    /**
     * Check if chosen route is valid
     * @param departure departure airport
     * @param arrival destination airport
     * @param trajectory chosen route
     * @return true if route exist or false otherwise
     */
    public boolean isValidRoute(int departure, int arrival, int trajectory) {
        HashMap<Integer, FlightReservation> routeAvailable = catalog.getFlightRouteAvailable(YDTCity.getValueCity(departure), YDTCity.getValueCity(arrival));
        if (routeAvailable.containsKey(trajectory)){
            return true;
        } return false;
    }

    /**
     * Check if the departure airport is different from the arrival airport
     * @param departure departure airport
     * @param arrival destination airport
     * @return true if the departure is different from the arrival, false otherwise
     */
    public boolean isValidFromDepartureToDestination(int departure, int arrival) {
        return YDTCity.getValueCity(departure) != null && YDTCity.getValueCity(arrival) != null && YDTCity.getValueCity(departure) != YDTCity.getValueCity(arrival);
    }

    /**
     * Check if chosen hotel is valid
     * @param arrival destination airport
     * @param hotelID chosen hotel identifier
     * @return true if the hotel is valid false otherwise
     */
    public boolean isValidHotel(int arrival, int hotelID) {
        HashMap<Integer, Hotel> hotels = catalog.returnHotelInDestination(YDTCity.getValueCity(arrival));
        for (Hotel hotel1 : hotels.values()) {
            if (hotel1.getHotelId() == hotelID) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if chosen bedroom is valid
     * @param arrival destination airport
     * @param hotelId chosen hotel identifier
     * @param bedID chosen bedroom id
     * @return true if chosen bed is valid false otherwise
     */
    public boolean isValidBedroom(int arrival, int hotelId, int bedID) {
        HashMap<Integer, Hotel> carRentCompanies = catalog.returnHotelInDestination(YDTCity.getValueCity(arrival));
        if (carRentCompanies.get(hotelId).getFreeBedrooms().containsKey(bedID)){
            return true;
        }
        return false;
    }

    /**
     * Check if chosen car rent company is valid
     * @param arrival destination airport
     * @param carRentCompanyID chosen car rent company identifier
     * @return true if chosen car rent company is valid false otherwise
     */
    public boolean isValidCarRentCompany(int arrival, int carRentCompanyID) {
        HashMap<Integer, CarRentCompany> carRentCompanies = catalog.returnCarRentCompanyInDestination(YDTCity.getValueCity(arrival));
        for (CarRentCompany carRentCompany : carRentCompanies.values()) {
            if (carRentCompany.getRentalCompanyID() == carRentCompanyID) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if chosen car is valid
     * @param arrival destination airport
     * @param rentCompanyID rent company id
     * @param carID chosen car identifier
     * @return true if chosen car is valid false otherwise
     */
    public boolean isValidCar(int arrival, int rentCompanyID, int carID) {
        HashMap<Integer, CarRentCompany> carRentCompanies = catalog.returnCarRentCompanyInDestination(YDTCity.getValueCity(arrival));
        if (carRentCompanies.get(rentCompanyID).getFreeCars().containsKey(carID)){
            return true;
        }
        return false;
    }



}




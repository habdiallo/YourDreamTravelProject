package org.example.domain.model.ValueObject.type;

import java.util.HashMap;
import java.util.Map;


/**
 * Enumerator for UML cities
 */
public enum YDTCity {
    PARIS(1){
        @Override
        public String toString() {
            return "Paris";
        }
    },
    BORDEAUX(2){
        @Override
        public String toString() {
            return "Bordeaux";
        }
    },
    CANBERRA(3){
        @Override
        public String toString() {
            return "Canberra";
        }
    },
    TOKYO(4){
        @Override
        public String toString() {
            return "Tokyo";
        }
    },
    DELHI(5) {
    @Override
    public String toString() {
        return "Delhi";
        }
    };

    private final static Map<Integer, YDTCity> REVERSE_MAP = new HashMap<>();

    private final Integer value;

    static {
        for (YDTCity status: values()) {
            REVERSE_MAP.put(status.value, status);
        }
    }

    YDTCity(Integer value) {
        this.value = value;
    }

    public static YDTCity getValueCity(int value) {
        return REVERSE_MAP.get(value);
    }

}

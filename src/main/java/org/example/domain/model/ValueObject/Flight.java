package org.example.domain.model.ValueObject;

import org.example.domain.exception.YourDreamTravelException;
import org.example.domain.model.ValueObject.type.YDTCity;
import org.example.domain.model.ValueObject.type.YDTDateFormat;

import java.text.NumberFormat;
import java.util.Objects;

public class Flight {
    private final int flightID;
    private final double flightPrice;
    private final String flightName;

    private final YDTCity departure;
    private final YDTCity destination;

    private final YDTDateFormat flightDates;

    private static int idCounter = 1;


    public int getFlightID() {
        return flightID;
    }

    public double getFlightPrice() {
        return flightPrice;
    }

    public String getFlightName() {
        return flightName;
    }

    public YDTCity getDeparture() {
        return departure;
    }

    public YDTCity getDestination() {
        return destination;
    }

    public YDTDateFormat getFlightDates() {
        return flightDates;
    }

    public Flight(String flightName, YDTCity departure, YDTCity destination, double flightPrice, YDTDateFormat flightDates) {
        if (departure == destination) throw new YourDreamTravelException(        "the departure must be different to the destination");
        this.flightName = flightName;
        this.departure = departure;
        this.destination = destination;
        this.flightPrice = flightPrice;
        this.flightDates = flightDates;
        this.flightID = idCounter;
        idCounter++;
    }

    @Override
    public String toString() {
        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(2); // two digits after the decimal point
        return  "Flight ID= " + flightID +
                ", " +flightName +
                ", from=" + departure +
                ", to=" + destination +
                ", date=" + flightDates +
                ", Price = " + format.format((flightPrice)) + " €";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flight flight)) return false;
        return Double.compare(flight.getFlightPrice(), getFlightPrice()) == 0
                && Objects.equals(getFlightID(), flight.getFlightID())
                && Objects.equals(getFlightName(), flight.getFlightName())
                && getDeparture() == flight.getDeparture()
                && getDestination() == flight.getDestination()
                && Objects.equals(getFlightDates(), flight.getFlightDates());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFlightID(), getFlightPrice(), getFlightName(), getDeparture(), getDestination(), getFlightDates());
    }
}

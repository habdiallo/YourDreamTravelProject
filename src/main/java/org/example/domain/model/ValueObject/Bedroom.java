package org.example.domain.model.ValueObject;

import java.util.Objects;

public class Bedroom {
    private final double priceOfBedroom;
    private final int bedroomId;
    private static int number = 0;

    public double getPriceOfBedroom() {
        return priceOfBedroom;
    }

    public int getBedroomId() {
        return bedroomId;
    }

    public Bedroom(double price) {
        this.priceOfBedroom = price;
        this.bedroomId = number;
        number++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bedroom bedroom)) return false;
        return Double.compare(bedroom.getPriceOfBedroom(), getPriceOfBedroom()) == 0 && getBedroomId() == bedroom.getBedroomId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPriceOfBedroom(), getBedroomId());
    }

    @Override
    public String toString() {
        return "         " + bedroomId + "- " + "Bedroom id=" + bedroomId + ", Bedroom Price=" + priceOfBedroom + "\n";
    }
}



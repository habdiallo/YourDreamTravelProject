package org.example.domain.model.ValueObject;

import java.util.Objects;

public class Car {
    private final int carID;
    private final double carRentalPrice;

    private final String model;

    private static int idCounter = 1;

    public String getModel() {
        return model;
    }

    public int getCarID() {
        return carID;
    }


    public double getCarRentalPrice() {
        return carRentalPrice;
    }

    public Car(String model, double carRentalPrice) {
        this.model = model;
        this.carRentalPrice = carRentalPrice;
        this.carID = idCounter;
        idCounter++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car car)) return false;
        return Objects.equals(getCarID(), car.getCarID());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCarID(), getCarRentalPrice());
    }

    @Override
    public String toString() {
        return "         " + carID + "- " + "car id=" + carID + ", marque=" + model + ", car Price=" + carRentalPrice + "\n";
    }
}

package org.example.domain.model.ValueObject.type;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Reference creating for customers
 */
public class YDTReferenceFormat {
    private final String id;

    public YDTReferenceFormat() {
        this.id = createID();
    }
    public String getId() {
        return id;
    }

    public static String createID(){
        AtomicLong atomicCounter = new AtomicLong();
        String currentCounter = String.valueOf(atomicCounter.getAndIncrement());
        String uniqueId = UUID.randomUUID().toString();
        Date todayDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyy");
        String s = uniqueId + "-" + currentCounter;
        return formatter.format(todayDate) + "-" + s.substring(0, 5).toUpperCase(Locale.ROOT);
    }

    @Override
    public String toString() {
        return id;
    }
}

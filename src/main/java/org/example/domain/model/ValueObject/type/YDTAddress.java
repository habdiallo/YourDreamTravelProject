package org.example.domain.model.ValueObject.type;

import org.example.domain.exception.YourDreamTravelException;

import java.util.Objects;

/**
 * Value Object (Type of Hotels or Car Rent Companies addresses)
 * @param city city in our enum class
 */
public record YDTAddress(YDTCity city) {

    public YDTAddress(YDTCity city) {
        if (city != null) {
            this.city = city;
        } else throw new YourDreamTravelException("City must not null");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof YDTAddress that)) return false;
        return city() == that.city();
    }

    @Override
    public int hashCode() {
        return Objects.hash(city());
    }

    @Override
    public String toString() {
        return "city=" + city;
    }
}

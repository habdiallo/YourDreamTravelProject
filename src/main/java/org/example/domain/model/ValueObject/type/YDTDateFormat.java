package org.example.domain.model.ValueObject.type;

import org.example.domain.exception.YourDreamTravelException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Formats dates
 * @param date (String)
 */
public record YDTDateFormat(String date) {
    public YDTDateFormat(String date) {
        this.date = stringToDate(date);
    }

    private static String stringToDate(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);
        formatter.setTimeZone(TimeZone.getTimeZone("France/Paris"));
        formatter.setLenient(false);
        try {
            Date date1 = formatter.parse(date);
            return formatter.format(date1);
        } catch (ParseException e) {
            throw new YourDreamTravelException(date + " Date format is invalid");
        }
    }


    @Override
    public String toString() {
        return date();
    }


}

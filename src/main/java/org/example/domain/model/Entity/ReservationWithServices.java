package org.example.domain.model.Entity;

import org.example.domain.model.Aggregate.Reservation;
import org.example.domain.model.Entity.service.FlightReservation;

import java.text.NumberFormat;

/**
 * class that create and manage the reservation with services.
 */
public class ReservationWithServices extends Reservation {
    private final Service service;

    public ReservationWithServices(Customer customer, FlightReservation flightReservation, Service service) {
        super(customer, flightReservation);
        this.service = service;
    }

    @Override
    public double getFlightReservationPrice() {
        double prix = service.getServicePrice() ;
                prix += super.getFlightReservationPrice();
        return prix;
    }



    @Override
    public String toString() {
        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(2);
        return super.toString() + service + "         Prix TTC (billet d'avion, hotel, voiture)=" + format.format((getFlightReservationPrice())) + " €";
    }
}

package org.example.domain.model.Entity;

import org.example.domain.exception.YourDreamTravelException;

import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

/**
 * class that create and manage the customer.
 */
public class Customer {
    private final String idClient;
    private final String name;

    public String getIdClient() {
        return idClient;
    }

    public String getName() {
        return name;
    }

    public Customer(String name) {
        if (name == null) {
            throw new YourDreamTravelException("name cannot be null");
        }
        if (!name.isEmpty() && !name.isBlank()){
            if (name.length() <= 20) {
                this.name = name;
            }
            else throw new YourDreamTravelException("Name must be less than 20 characters");
        } else throw new YourDreamTravelException("Firstname cannot be empty");
        this.idClient = createID();
    }

    /**
     * Method to create an id for a customer
     * @return idClient
     */
    private String createID() {
        String firstName = getName();
        String uniqueId = UUID.randomUUID().toString();
        String s = firstName.toUpperCase() + uniqueId.toUpperCase(Locale.ROOT);
        if (s.length() > 8) {
            return s.substring(0,8);
        }
        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer customer)) return false;
        return Objects.equals(getIdClient(), customer.getIdClient())
                && Objects.equals(getName(), customer.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdClient(), getName());
    }

    @Override
    public String toString() {
        return "id du client=" + idClient +
                ", name=" + name;
    }
}

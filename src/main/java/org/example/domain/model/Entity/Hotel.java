package org.example.domain.model.Entity;

import org.example.domain.model.Entity.service.BedroomReservation;
import org.example.domain.model.ValueObject.type.YDTAddress;

import java.util.HashMap;
import java.util.Objects;

/**
 * class that create and manage the hotels.
 */
public class Hotel {
    private final String hotelName;
    private final HashMap<Integer, BedroomReservation> bedrooms;
    private final YDTAddress hotelAddress;
    private final int hotelId;
    private static int idCounter = 1;


    public String getHotelName() {
        return hotelName;
    }

    public HashMap<Integer, BedroomReservation> getBedrooms() {
        return bedrooms;
    }

    public YDTAddress getHotelAddress() {
        return hotelAddress;
    }

    public int getHotelId() {
        return hotelId;
    }

    public Hotel(String hotelName, YDTAddress hotelAddress, HashMap<Integer, BedroomReservation> bedrooms) {
        this.hotelName = hotelName;
        this.bedrooms = bedrooms;
        this.hotelAddress = hotelAddress;
        this.hotelId = idCounter;
        idCounter++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hotel hotel)) return false;
        return getHotelId() == hotel.getHotelId() && Objects.equals(getHotelName(), hotel.getHotelName()) && Objects.equals(getBedrooms(), hotel.getBedrooms()) && Objects.equals(getHotelAddress(), hotel.getHotelAddress());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHotelName(), getBedrooms(), getHotelAddress(), getHotelId());
    }

    public HashMap<Integer, BedroomReservation> getFreeBedrooms() {
        HashMap<Integer, BedroomReservation> freeBedrooms = new HashMap<>();
        for (BedroomReservation freeBedRoom : bedrooms.values()){
            if (freeBedRoom.getNumberOfDays() == 0){
                freeBedrooms.put(freeBedRoom.getId(), freeBedRoom);
            }
        }
        return freeBedrooms;
    }

    @Override
    public String toString() {
        return "          " + hotelId + "- " + "Hotel Id= " + hotelId + ", name= " + hotelName + "address= " + hotelAddress + "\n";
    }
}

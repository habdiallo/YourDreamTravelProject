package org.example.domain.model.Entity.service;

import org.example.domain.model.ValueObject.Flight;

import java.text.NumberFormat;
import java.util.ArrayList;

public class FlightReservation {

    private final int trajectoryID;
    private final ArrayList<Flight> flights;

    private double flightReservationPrice;
    public double getFlightReservationPrice() {
        return flightReservationPrice;
    }

    public int getTrajectoryID() {
        return trajectoryID;
    }

    public ArrayList<Flight> getFlights() {
        return flights;
    }

    public FlightReservation(int id, ArrayList<Flight> flights) {
        this.trajectoryID = id;
        this.flights = flights;
        totalPrice(flights);
    }

    private void totalPrice(ArrayList<Flight> flights) {
        flightReservationPrice = 0.0;
        for (Flight flight : flights) {
            flightReservationPrice += flight.getFlightPrice();
        }
    }

    @Override
    public String toString() {
        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(2); // two digits after the decimal point
        StringBuilder sb = new StringBuilder();
        sb.append("         ").append(trajectoryID).append("- ").append("Trajet : " + " ID Trajet : ").append(trajectoryID).append(" ");
        for(Flight flight : flights) {
            sb.append(" from ").append(flight.getDeparture()).append(", to ").append(flight.getDestination()).append(", date= ").append(flight.getFlightDates());
        }
        sb.append(", prix reservation billet d'avion= ").append(format.format((flightReservationPrice))).append(" €").append("\n");
        return sb.toString();
    }
}

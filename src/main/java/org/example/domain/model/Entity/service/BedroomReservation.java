package org.example.domain.model.Entity.service;

import org.example.domain.model.ValueObject.Bedroom;

import java.text.NumberFormat;

public class BedroomReservation {
    private final int id;
    private final Bedroom bedroom;
    private int numberOfDays;

    private static int idCounter = 0;

    private double bedroomReservationPrice;

    public int getId() {
        return id;
    }

    public Bedroom getBedroom() {
        return bedroom;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public double getBedroomReservationPrice() {
        return bedroomReservationPrice;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public BedroomReservation(Bedroom bedroom, int numberOfDays) {
        this.bedroom = bedroom;
        this.numberOfDays = numberOfDays;
        totalPrice(numberOfDays);
        this.id = idCounter;
        idCounter++;
    }

    private void totalPrice(int numberOfDays) {
        bedroomReservationPrice += bedroom.getPriceOfBedroom() * numberOfDays;
    }

    @Override
    public String toString() {
        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(2); // two digits after the decimal point
        StringBuilder sb = new StringBuilder();
        sb.append(bedroom);
        if (numberOfDays > 0){
            sb.append("                 nombre de jour=").append(numberOfDays).append(", prix reservation chambre =").append(format.format((bedroomReservationPrice))).append(" €").append("\n");
        }
        return sb.toString();
    }
}

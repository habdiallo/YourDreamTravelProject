package org.example.domain.model.Entity.service;

import org.example.domain.model.ValueObject.Car;

import java.text.NumberFormat;

public class CarReservation {
    private final int id;
    private final Car car;
    private int numberOfDays;

    private static int idCounter = 0;

    private double carReservationPrice;

    public int getId() {
        return id;
    }

    public Car getCar() {
        return car;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public double getCarReservationPrice() {
        return carReservationPrice;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public CarReservation(Car car, int numberOfDays) {
        this.car = car;
        this.numberOfDays = numberOfDays;
        totalPrice(numberOfDays);
        this.id = idCounter;
        idCounter++;
    }

    private void totalPrice(int numberOfDays) {
        carReservationPrice += car.getCarRentalPrice() * numberOfDays;
    }

    @Override
    public String toString() {
        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(2); // two digits after the decimal point
        StringBuilder sb = new StringBuilder();
        sb.append(car);
        if (numberOfDays <= 0) {
            return sb.toString();
        }
        sb.append("                 nombre de jour=").append(numberOfDays).append(", prix location voiture=").append(format.format((carReservationPrice))).append(" €").append("\n");
        return sb.toString();
    }
}

package org.example.domain.model.Entity;

import java.util.HashMap;

/**
 * class that create and manage the Deluxe services.
 */
public class DeluxeService extends Service {
    private final int deluxeServiceID;

    private static int idCounter = 1;
    private final HashMap<Integer, SimpleService> idSimpleServicesHashMap;

    public DeluxeService(HashMap<Integer, SimpleService> services) {
        this.deluxeServiceID = idCounter;
        this.idSimpleServicesHashMap = services;
        idCounter++;
    }

    @Override
    public int getServiceId() {
        return deluxeServiceID;
    }

    @Override
    public double getServicePrice() {
        double price = 0.0;
        for (SimpleService simpleService : idSimpleServicesHashMap.values()) {
            price += simpleService.getServicePrice();
        }
        return price;
    }

    @Override
    public String toString() {
        return "DeluxeService{" +
                "deluxeServiceID=" + deluxeServiceID +
                ", idSimpleServicesHashMap=" + idSimpleServicesHashMap +
                '}';
    }
}

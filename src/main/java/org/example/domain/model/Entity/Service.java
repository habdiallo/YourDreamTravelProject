package org.example.domain.model.Entity;

import java.text.NumberFormat;

/**
 * abstract class that create and manage the service.
 */
public abstract class Service {
    public abstract int getServiceId();
    public abstract double getServicePrice();


    public String toString() {
        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(2); // two digits after the decimal point
        return "         Prix du service=" + format.format((getServicePrice())) + "€" + "\n";
    }

}

package org.example.domain.model.Entity;

import org.example.domain.model.ValueObject.Bedroom;
import org.example.domain.model.ValueObject.Car;
import org.example.domain.model.Entity.service.BedroomReservation;
import org.example.domain.model.Entity.service.CarReservation;

/**
 * class that create and manage the simple service.
 */
public class SimpleService extends Service {

    private final int serviceID;
    private final double servicePrice;
    private final BedroomReservation bedroom;
    private final CarReservation car;

    private static int idCounter = 0;

    public int getServiceID() {
        return serviceID;
    }

    public Bedroom getBedroom() {
        return bedroom.getBedroom();
    }

    public Car getCar() {
        return car.getCar();
    }

    public SimpleService(BedroomReservation bedroom, CarReservation car) {
        this.servicePrice = car.getCarReservationPrice() + bedroom.getBedroomReservationPrice();
        this.bedroom = bedroom;
        this.car = car;
        this.serviceID = idCounter;
        idCounter++;
    }

    @Override
    public int getServiceId() {
        return serviceID;
    }

    @Override
    public double getServicePrice() {
        return servicePrice;
    }

    @Override
    public String toString() {
        return bedroom + "\n" + car + super.toString();
   }
}

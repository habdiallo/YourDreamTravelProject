package org.example.domain.model.Entity;

import org.example.domain.model.Entity.service.CarReservation;
import org.example.domain.model.ValueObject.type.YDTAddress;

import java.util.HashMap;

/**
 * class that create and manage the Car Rent Company.
 */
public class CarRentCompany {
    private final int rentalCompanyID;
    String rentalCompanyName;
    YDTAddress rentalCompanyAddress;
    private final HashMap<Integer, CarReservation> cars;
    private static int idCounter = 1;

    public int getRentalCompanyID() {
        return rentalCompanyID;
    }

    public String getRentalCompanyName() {
        return rentalCompanyName;
    }

    public YDTAddress getRentalCompanyAddress() {
        return rentalCompanyAddress;
    }

    public HashMap<Integer, CarReservation> getCars() {
        return cars;
    }

    public CarRentCompany(String rentalCompanyName, YDTAddress rentalCompanyAddress, HashMap<Integer, CarReservation> cars) {
        this.rentalCompanyName = rentalCompanyName;
        this.rentalCompanyAddress = rentalCompanyAddress;
        this.rentalCompanyID = idCounter;
        idCounter++;
        this.cars = cars;
    }

    /**
     * Returns the free cars
     * @return free cars
     */
    public HashMap<Integer, CarReservation> getFreeCars() {
        HashMap<Integer, CarReservation> freeCars = new HashMap<>();
        for (CarReservation freeBedCar : cars.values()){
            if (freeBedCar.getNumberOfDays() == 0){
                freeCars.put(freeBedCar.getId(), freeBedCar);
            }
        }
        return freeCars;
    }

    @Override
    public String toString() {
        return "          " + rentalCompanyID + "- " + "Company Id= " + rentalCompanyID + ", name= " + rentalCompanyName + "address= " + rentalCompanyAddress + "\n";
    }
}

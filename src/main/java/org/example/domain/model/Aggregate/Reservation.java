package org.example.domain.model.Aggregate;

import org.example.domain.model.Entity.Customer;
import org.example.domain.model.Entity.service.FlightReservation;
import org.example.domain.model.ValueObject.type.YDTDateFormat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;


/**
 *  This class belongs to the aggregate
 */
public class Reservation {
    private final int reservationID;
    private final Customer customer;
    private final YDTDateFormat reservationDate;
    private final FlightReservation trajectory;

    private static int id = 1;
    public int getReservationID() {
        return reservationID;
    }

    public Customer getCustomer() {
        return customer;
    }

    public YDTDateFormat getReservationDate() {
        return reservationDate;
    }

    public FlightReservation getTrajectory() {
        return trajectory;
    }

    public double getFlightReservationPrice() {
        return trajectory.getFlightReservationPrice();
    }

    public Reservation(Customer customer, FlightReservation trajectory) {
        this.customer = customer;
        this.reservationDate = new YDTDateFormat(createTodayDate());
        this.trajectory = trajectory;
        this.reservationID = id;
        id++;
    }

    /**
     * Define the date of the reservation
     * @return today date
     */
    public String createTodayDate(){
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);
        return dateFormat.format(date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reservation that)) return false;
        return Objects.equals(getReservationID(), that.getReservationID());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getReservationID());
    }

    @Override
    public String toString() {
        return "         " + "Reservation details :" + " id de la reservation=" + reservationID + ", Date de la reservation= " + reservationDate + "\n" +
                "         " + customer + "\n" +
                trajectory + "\n";
    }

}

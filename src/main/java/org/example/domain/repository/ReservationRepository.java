package org.example.domain.repository;

import org.example.domain.model.Aggregate.Reservation;

public interface ReservationRepository {
    public Reservation findReservationById(int id);
    public void save(Reservation reservation);

}

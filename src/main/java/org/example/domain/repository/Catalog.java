package org.example.domain.repository;

import org.example.domain.model.Entity.CarRentCompany;
import org.example.domain.model.Entity.Hotel;
import org.example.domain.model.ValueObject.Bedroom;
import org.example.domain.model.ValueObject.Car;
import org.example.domain.model.ValueObject.Flight;
import org.example.domain.model.Entity.service.BedroomReservation;
import org.example.domain.model.Entity.service.CarReservation;
import org.example.domain.model.Entity.service.FlightReservation;
import org.example.domain.model.ValueObject.type.YDTAddress;
import org.example.domain.model.ValueObject.type.YDTCity;
import org.example.domain.model.ValueObject.type.YDTDateFormat;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;

import static org.example.domain.model.ValueObject.type.YDTCity.*;

/**
 * class that create and manage the catalog for our test.
 */
public class Catalog {
    private HashMap<Integer, Hotel> hotels;
    private HashMap<Integer, CarRentCompany> carRentCompanies;
    private HashMap<Integer, Flight> flights;

    private final EnumMap<YDTCity, ArrayList<YDTCity>> fromDepartureAirportToArrivalAirport;

    public HashMap<Integer, Hotel> getHotels() {
        return hotels;
    }

    public HashMap<Integer, CarRentCompany> getCarRentCompanies() {
        return carRentCompanies;
    }

    public HashMap<Integer, Flight> getFlights() {
        return flights;
    }

    public EnumMap<YDTCity, ArrayList<YDTCity>> getFromDepartureAirportToArrivalAirport() {
        return fromDepartureAirportToArrivalAirport;
    }

    public Catalog() {
        this.hotels = createHotels();;
        this.carRentCompanies = createRentalCompanies();
        this.flights = createFlights();
        this.fromDepartureAirportToArrivalAirport = matchFromDepartureAirportToArrivalAirport();
    }

    public Catalog initializeCatalog() {
        this.hotels = new HashMap<>();
        this.carRentCompanies = new HashMap<>();
        this.flights = new HashMap<>();
        return this;
    }

    HashMap<Integer, CarRentCompany> createRentalCompanies() {
        Car car1 = new Car("BMW", 20);
        Car car2 = new Car("TOYOTA", 18);
        Car car3 = new Car("KIA", 16);
        Car car4 = new Car("CITROEN", 14);
        Car car5 = new Car("RENAULT", 12);

        CarReservation carReservation1 = new CarReservation(car1, 0);
        CarReservation carReservation2 = new CarReservation(car2, 0);
        CarReservation carReservation3 = new CarReservation(car3, 0);
        CarReservation carReservation4 = new CarReservation(car4, 0);
        CarReservation carReservation5 = new CarReservation(car5, 0);


        HashMap<Integer, CarReservation> cars = new HashMap<>();
        cars.put(carReservation1.getId(), carReservation1);
        cars.put(carReservation2.getId(), carReservation2);
        cars.put(carReservation3.getId(), carReservation3);
        cars.put(carReservation4.getId(), carReservation4);
        cars.put(carReservation5.getId(), carReservation5);


        CarRentCompany companyPAR = new CarRentCompany("Paris Location", new YDTAddress(PARIS), cars);
        CarRentCompany companyBDX = new CarRentCompany("Bordeaux Location", new YDTAddress(BORDEAUX), cars);
        CarRentCompany companyTKO = new CarRentCompany("Tokyo Location", new YDTAddress(TOKYO), cars);
        CarRentCompany companyCBA = new CarRentCompany("Camberra Location", new YDTAddress(CANBERRA), cars);
        CarRentCompany companyDLH = new CarRentCompany("Delhi Location", new YDTAddress(DELHI), cars);
        HashMap<Integer, CarRentCompany> carRentCompanies = new HashMap<Integer, CarRentCompany>();
        carRentCompanies.put(companyPAR.getRentalCompanyID(), companyPAR);
        carRentCompanies.put(companyBDX.getRentalCompanyID(), companyBDX);
        carRentCompanies.put(companyTKO.getRentalCompanyID(), companyTKO);
        carRentCompanies.put(companyCBA.getRentalCompanyID(), companyCBA);
        carRentCompanies.put(companyDLH.getRentalCompanyID(), companyDLH);
        return carRentCompanies;
    }

    private HashMap<Integer, Hotel> createHotels() {
        Bedroom bed1 = new Bedroom(80);
        Bedroom bed2 = new Bedroom(70);
        Bedroom bed3 = new Bedroom(50);
        Bedroom bed4 = new Bedroom(40);
        Bedroom bed5 = new Bedroom(30);

        BedroomReservation bedReservation1 = new BedroomReservation(bed1, 0);
        BedroomReservation bedReservation2 = new BedroomReservation(bed2, 0);
        BedroomReservation bedReservation3 = new BedroomReservation(bed3, 0);
        BedroomReservation bedReservation4 = new BedroomReservation(bed4, 0);
        BedroomReservation bedReservation5 = new BedroomReservation(bed5, 0);

        HashMap<Integer, BedroomReservation> bedrooms = new HashMap<>();
        bedrooms.put(bedReservation1.getId(), bedReservation1);
        bedrooms.put(bedReservation2.getId(), bedReservation2);
        bedrooms.put(bedReservation3.getId(), bedReservation3);
        bedrooms.put(bedReservation4.getId(), bedReservation4);
        bedrooms.put(bedReservation5.getId(), bedReservation5);
        Hotel hotelPAR = new Hotel("Paris Hotel", new YDTAddress(PARIS), bedrooms);
        Hotel hotelBDX = new Hotel("Bordeaux Hotel", new YDTAddress(BORDEAUX), bedrooms);
        Hotel hotelTKO = new Hotel("Tokyo Hotel", new YDTAddress(TOKYO), bedrooms);
        Hotel hotelCBA = new Hotel("Camberra Hotel", new YDTAddress(CANBERRA), bedrooms);
        Hotel hotelDLH = new Hotel("Delhi Hotel", new YDTAddress(DELHI), bedrooms);
        HashMap<Integer, Hotel> hotels = new HashMap<Integer, Hotel>();
        hotels.put(hotelPAR.getHotelId(),hotelPAR);
        hotels.put(hotelBDX.getHotelId(),hotelBDX);
        hotels.put(hotelTKO.getHotelId(),hotelTKO);
        hotels.put(hotelCBA.getHotelId(),hotelCBA);
        hotels.put(hotelDLH.getHotelId(),hotelDLH);
        return hotels;
    }

    private HashMap<Integer, Flight> createFlights(){
        YDTDateFormat date1 = new YDTDateFormat("10/01/2023");
        YDTDateFormat date2 = new YDTDateFormat("10/02/2023");
        YDTDateFormat date3 = new YDTDateFormat("10/03/2023");
        YDTDateFormat date4 = new YDTDateFormat("10/05/2023");
        YDTDateFormat date5 = new YDTDateFormat("10/06/2023");

        Flight flight1 = new Flight("AirFrance", PARIS, BORDEAUX, 20, date1);
        Flight flight2 = new Flight("AirFrance", PARIS, CANBERRA, 100, date1);
        Flight flight3 = new Flight("AirFrance", PARIS, TOKYO, 200, date1);
        Flight flight4 = new Flight("AirFrance", PARIS, DELHI, 400, date1);

        Flight flight5 = new Flight("AirPortugal", BORDEAUX, PARIS, 30, date1);
        Flight flight6 = new Flight("AirPortugal", BORDEAUX, TOKYO, 500, date1);
        Flight flight7 = new Flight("AirPortugal", BORDEAUX, DELHI, 490, date2);

        Flight flight8 = new Flight("BrusselsAirlines", CANBERRA, PARIS, 600, date2);
        Flight flight9 = new Flight("BrusselsAirlines", CANBERRA, TOKYO, 540, date3);
        Flight flight10 = new Flight("BrusselsAirlines", CANBERRA, DELHI, 900, date3);

        Flight flight11 = new Flight("Pegasus", TOKYO, BORDEAUX, 480, date4);
        Flight flight12 = new Flight("Pegasus", TOKYO, CANBERRA, 390, date4);

        Flight flight13 = new Flight("TurkishAirline", DELHI, PARIS, 460, date5);
        Flight flight14 = new Flight("TurkishAirline", DELHI, BORDEAUX, 600, date5);
        Flight flight15 = new Flight("TurkishAirline", DELHI, CANBERRA, 700, date5);



        HashMap<Integer, Flight> flights = new HashMap<>();
        flights.put(flight1.getFlightID(), flight1);
        flights.put(flight2.getFlightID(), flight2);
        flights.put(flight3.getFlightID(), flight3);
        flights.put(flight4.getFlightID(), flight4);
        flights.put(flight5.getFlightID(), flight5);
        flights.put(flight6.getFlightID(), flight6);
        flights.put(flight7.getFlightID(), flight7);
        flights.put(flight8.getFlightID(), flight8);
        flights.put(flight9.getFlightID(), flight9);
        flights.put(flight10.getFlightID(), flight10);
        flights.put(flight11.getFlightID(), flight11);
        flights.put(flight12.getFlightID(), flight12);
        flights.put(flight13.getFlightID(), flight13);
        flights.put(flight14.getFlightID(), flight14);
        flights.put(flight15.getFlightID(), flight15);
        return flights;
    }


    /**
     * Definition of possible direct flights
     * @return a departure airport and its possible direct destination list according to the table provided in the UML tutorial
     */
    private EnumMap<YDTCity, ArrayList<YDTCity>> matchFromDepartureAirportToArrivalAirport() {
        ArrayList<YDTCity> parisDestinationList = new ArrayList<>();
        parisDestinationList.add(BORDEAUX);
        parisDestinationList.add(CANBERRA);
        parisDestinationList.add(DELHI);
        parisDestinationList.add(TOKYO);

        ArrayList<YDTCity> bordeauxDestinationList = new ArrayList<>();
        bordeauxDestinationList.add(PARIS);
        bordeauxDestinationList.add(TOKYO);
        bordeauxDestinationList.add(DELHI);

        ArrayList<YDTCity> camberraDestinationList = new ArrayList<>();
        camberraDestinationList.add(PARIS);
        camberraDestinationList.add(TOKYO);

        ArrayList<YDTCity> tokyoDestinationList = new ArrayList<>();
        tokyoDestinationList.add(BORDEAUX);
        tokyoDestinationList.add(CANBERRA);

        ArrayList<YDTCity> delhiDestinationList = new ArrayList<>();
        delhiDestinationList.add(PARIS);
        delhiDestinationList.add(BORDEAUX);
        delhiDestinationList.add(CANBERRA);

        return new EnumMap<>(YDTCity.class) {{
            put(PARIS, parisDestinationList);
            put(BORDEAUX, bordeauxDestinationList);
            put(CANBERRA, camberraDestinationList);
            put(TOKYO, tokyoDestinationList);
            put(DELHI, delhiDestinationList);

        }};
    }

    /**
     * Returns the list of available rent companies for the given destination
     * @param city destination
     * @return list of available rent companies
     */
    public HashMap<Integer, CarRentCompany> returnCarRentCompanyInDestination(YDTCity city){
        HashMap<Integer, CarRentCompany> carRentList = new HashMap<>();
        for (CarRentCompany carRentCompany: getCarRentCompanies().values()){
            if (carRentCompany.getRentalCompanyAddress().city().equals(city)){
                carRentList.put(carRentCompany.getRentalCompanyID(), carRentCompany);            }
        }
        return new HashMap<>(carRentList);
    }

    /**
     * Returns the list of available hotels for the given destination
     * @param city destination
     * @return list of available hotel
     */
    public HashMap<Integer, Hotel> returnHotelInDestination(YDTCity city){
        HashMap<Integer, Hotel> hotelList = new HashMap<>();
        for (Hotel hotel: getHotels().values()){
            if (hotel.getHotelAddress().city().equals(city)){
                hotelList.put(hotel.getHotelId(), hotel);            }
        }
        return hotelList;
    }

    /**
     * Returns the list of available routes for the given departure and destination.
     * If there is a direct flight the connections are ignored
     * If there is no direct flight, connections are suggested if the two flights are of the same date.
     * When testing you can adjust the dates in the catalog as needed to see what happens.
     * @param departureAirport departure airport
     * @param arrivalAirport destination airport
     * @return list of available routes
     */
    public HashMap<Integer, FlightReservation> getFlightRouteAvailable(YDTCity departureAirport, YDTCity arrivalAirport){
        ArrayList<Flight> flightDeparture = new ArrayList<>();
        ArrayList<Flight> flightArrival = new ArrayList<>();
        HashMap<Integer, FlightReservation> routeAvailable = new HashMap<>();
        int idRouteTrajectory=0;
        for (Flight flight : flights.values()) {
            if (flight.getDeparture().equals(departureAirport) && flight.getDestination().equals(arrivalAirport)) {
                idRouteTrajectory++;
                FlightReservation route = new FlightReservation(idRouteTrajectory, new ArrayList<>() {{
                    add(flight);
                }});
                routeAvailable.put(route.getTrajectoryID(), route);
                break;
            }
            if (flight.getDeparture().equals(departureAirport)) {
                flightDeparture.add(flight);
            }
            if (flight.getDestination().equals(arrivalAirport)) {
                flightArrival.add(flight);
            }
        }
        for (Flight flight_bis : flightDeparture) {
            for (Flight flight1 : flightArrival) {
                if (flight_bis.getDestination().equals(flight1.getDeparture()) && flight1.getFlightDates().equals(flight_bis.getFlightDates())) {
                    idRouteTrajectory++;
                    FlightReservation route1 = new FlightReservation(idRouteTrajectory,new ArrayList<>(){{add(flight_bis);add(flight1);}});
                    routeAvailable.put(route1.getTrajectoryID(), route1);
                }
            }
        }
        return routeAvailable;
    }

    @Override
    public String toString() {
        return  "Catalog \n" +
                hotels +
                carRentCompanies +
                flights +
                ", fromDepartureAirportToArrivalAirport=" + fromDepartureAirportToArrivalAirport +
                '}';
    }

}

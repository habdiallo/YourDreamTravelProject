package org.example.domain.exception;

public class YourDreamTravelException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    public YourDreamTravelException(String msg) {
        super(msg);
    }
}

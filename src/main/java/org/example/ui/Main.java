package org.example.ui;

import java.io.IOException;

/**
 * @author habdiallo and mahamat
 */
public class Main {
    public static void main(String[] args) {
        TextualMenu menu = new TextualMenu();
        menu.handleUserInstructions();
    }
}
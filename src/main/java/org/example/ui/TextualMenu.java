package org.example.ui;

import org.example.application.ReservationApp;
import org.example.domain.exception.YourDreamTravelException;

import java.io.*;
import java.util.Objects;
import java.util.Scanner;

import static java.lang.System.out;

public class TextualMenu {
    ReservationApp rServices;

    public TextualMenu() {
        this.rServices = new ReservationApp();
    }

    public void handleUserInstructions() {
        printMenuPrincipal();
        String choice = saisieChaine();
        while (true) {
            assert choice != null;
            if (choice.equals("q")) break;
            if (choice.equals("1")) {
                createReservation();
            } else {
                out.println("choice incorrect!");
            }
            printMenuPrincipal();
            choice = saisieChaine();
        }
    }

    private void createReservation() {
        Scanner scanner = new Scanner(System.in);
        out.println("-------------------------------- S'enregistrer --------------------------------");
        out.println("Saisir le nom");
        String name = saisieChaine();
        try {
            printMenuChoiceReservationType();
            String choice = saisieChaine();
            if (Objects.equals(choice, "1")) {
                out.println("-------------------------------- Choisissez une ville de depart --------------------------------");
                int departure = readInt(scanner, rServices.printDeparture(), "Ceci n'est pas un nombre entier recommencez: \n");
                out.println("-------------------------------- Choisissez une ville de destination --------------------------------");
                int arrival = readInt(scanner, rServices.printDeparture(), "Ceci n'est pas un nombre entier recommencez: \n");
                if (rServices.isValidFromDepartureToDestination(departure, arrival)){
                    out.println("-------------------------------- Choisissez un trajet de votre choix --------------------------------");
                    int trajectory = readInt(scanner, rServices.printAvailableRoute(departure, arrival), "Ceci n'est pas un nombre entier recommencez: \n");
                    if (rServices.isValidRoute(departure, arrival, trajectory)){
                        int reservation = rServices.createReservationWithoutService(name, departure, arrival, trajectory);
                        rServices.printReservation(reservation);
                    } else out.println("           La trajet choisie n'existe pas");
                }
            }
            else {
                assert choice != null;
                switch (choice) {
                    case "2" -> {
                        out.println("-------------------------------- Choisissez une ville de depart --------------------------------");
                        int departure = readInt(scanner, rServices.printDeparture(), "Ceci n'est pas un nombre entier recommencez: \n");
                        out.println("-------------------------------- Choisissez une ville de destination --------------------------------");
                        int arrival = readInt(scanner, rServices.printDeparture(), "Ceci n'est pas un nombre entier recommencez: \n");
                        if (rServices.isValidFromDepartureToDestination(departure, arrival)) {
                            out.println("-------------------------------- Choisissez un trajet de votre choix --------------------------------");
                            int trajectory = readInt(scanner, rServices.printAvailableRoute(departure, arrival), "Ceci n'est pas un nombre entier recommencez: \n");
                            if (rServices.isValidRoute(departure, arrival, trajectory)) {
                                out.println("-------------------------------- Choisissez un hotel --------------------------------");
                                int selectedHotel = readInt(scanner, rServices.printAvailableHotelInDestination(arrival), "Ceci n'est pas un nombre entier recommencez: \n");
                                if (rServices.isValidHotel(arrival, selectedHotel)) {
                                    out.println("-------------------------------- Choisissez une chambre --------------------------------");
                                    int selectedBedroom = readInt(scanner, rServices.printAvailableBedroom(arrival, selectedHotel), "Ceci n'est pas un nombre entier recommencez: \n");
                                    out.println("-------------------------------- Choisissez Le nombre de jour --------------------------------");
                                    int numbDayBedroom = readInt(scanner, "         Entrez le nombre de jours que vous souhaitez rester\n", "         Ceci n'est pas un nombre entier recommencez: \n");
                                    if (rServices.isValidBedroom(arrival, selectedHotel, selectedBedroom)) {
                                        out.println("-------------------------------- Choisissez un loueur de voiture --------------------------------");
                                        int selectedCarRentCompany = readInt(scanner, rServices.printAvailableCarRentCompany(arrival), "Ceci n'est pas un nombre entier recommencez: \n");
                                        if (rServices.isValidCarRentCompany(arrival, selectedCarRentCompany)) {
                                            out.println("-------------------------------- Choisissez une voiture --------------------------------");
                                            int selectedCar = readInt(scanner, rServices.printAvailableCar(arrival, selectedCarRentCompany), "Ceci n'est pas un nombre entier recommencez: \n");
                                            out.println("-------------------------------- Choisissez Le nombre de jour --------------------------------");
                                            int numbDayCar = readInt(scanner, "         Entrez le nombre de jours que vous souhaitez louer la voiture\n", "         Ceci n'est pas un nombre entier recommencez: \n");
                                            if (rServices.isValidCar(arrival, selectedCarRentCompany, selectedCar)) {
                                                if (rServices.isValidRoute(departure, arrival, trajectory)) {
                                                    int reservation = rServices.createReservationWithServiceSimple(name, departure, arrival, trajectory, selectedHotel, selectedBedroom, numbDayBedroom, selectedCarRentCompany, selectedCar, numbDayCar);
                                                    out.println("-------------------------------- Merci pour la confiance que vous nous accordé, ci-dessous votre reçu ! --------------------------------");
                                                    rServices.printReservation(reservation);
                                                }
                                            } else
                                                out.println("         Le voiture selectionné n'existe pas ou n'est pas libre");
                                        } else out.println("           La compagnie de location choisie n'existe pas");
                                    } else out.println("           La chambre choisie n'existe pas");
                                } else out.println("           L'hotel choisie n'existe pas");
                            } else out.println("           La trajet choisie n'existe pas");
                        }
                    }
                    case "3" ->
                            out.println(" Le service à révoir si toutefois je comprends ce qu'on attend de se service");

                    //Todo reservation avec service haut de game;
                    case "r" -> printMenuPrincipal();
                    default -> out.println("choice incorrect!");
                }
            }
        } catch (YourDreamTravelException exception){
            out.println(exception.getMessage());
        }

    }


    /**
     * SAISIE DE CHAINE
     *
     * @return la chaine saisie
     */
    public static String saisieChaine() {
        try {
            BufferedReader buff = new BufferedReader
                    (new InputStreamReader(System.in));
            return buff.readLine();
        } catch (IOException e) {
            out.println(" impossible"
                    + e);
            return null;
        }
    }


    /**
     * Entering an integer
     * @param scanner the scanner
     * @param prompt the prompt
     * @param promptOnError the error
     * @return integer if it is or message error if is not an integer
     */
    public static int readInt(Scanner scanner, String prompt, String promptOnError) {

        System.out.print(prompt);

        while (!scanner.hasNextInt()) {
            System.out.print(promptOnError);
            scanner.nextLine(); // vidage saisie incorrect
        }

        final int input = scanner.nextInt();
        scanner.nextLine(); // vidage buffer
        return input;
    }

    /**
     * MENU PRINCIPAL
     */
    public static void printMenuPrincipal() {
        out.println("-------------------------------- Menu Principal --------------------------------");
        out.println("Bienvenue chez Your Dream Travel");
        out.println("          1- Pour Faire une reservation                                              ");
        out.println("          Q. Quitter                           ");
    }

    public static void printMenuChoiceReservationType() {
        out.println("-------------------------------- Choix du type de reservation --------------------------------");
        out.println("          1- Pour une reservation sans service                                ");
        out.println("          2- Une reservation avec un service simple                 ");
        out.println("          3- Une reservation avec un service haut de game                ");
        out.println("          r. Pour aller menu principale                           ");
    }
}


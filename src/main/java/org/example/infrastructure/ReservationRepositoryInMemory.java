package org.example.infrastructure;

import org.example.domain.model.Aggregate.Reservation;
import org.example.domain.repository.ReservationRepository;

import java.util.HashSet;
import java.util.Set;

public class ReservationRepositoryInMemory implements ReservationRepository {
    Set<Reservation> reservationMemory;

    public ReservationRepositoryInMemory() {
        this.reservationMemory = new HashSet<>();
    }


    @Override
    public Reservation findReservationById(int id) {
        for (Reservation reservation : reservationMemory) {
            if (reservation.getReservationID() == id) {
                return reservation;
            }
        }
        return null;
    }

    @Override
    public void save(Reservation reservation) {
        reservationMemory.add(reservation);
    }
}
